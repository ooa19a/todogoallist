//library code
function createStore(reducer) {
  // The store has the following information
  // 1. the state tree
  // 2. a way to get the  state tree
  // 3. a way to listen and respond to the state changing
  // 4. a way to update the state

  // What are Pure Functions?
  // Pure functions are integral to how state in Redux applications is updated. By definition, pure functions:

  // - Return the same result if the same arguments are passed in
  // - Depend solely on the arguments passed into them
  // - Do not produce side effects, such as API requests and I/O operations

  let state;
  let listeners = [];

  const getState = () => state;

  const subscribe = (listener) => {
    listeners.push(listener);
    return () => {
      listeners = listeners.filter((l) => l !== l);
    };
  };

  const dispatch = (action) => {
    state = reducer(state, action);
    listeners.forEach((listener) => listener());
  };

  return {
    getState,
    subscribe,
    dispatch,
  };
}

//App code
const ADD_TODO = "ADD_TODO";
const REMOVE_TODO = "REMOVE_TODO";
const TOOGLE_TODO = "TOOGLE_TODO";
const ADD_GOAL = "ADD_GOAL";
const REMOVE_GOAL = "REMOVE_GOAL";

//action creators

function addTodoAction(todo) {
  return {
    type: ADD_TODO,
    todo,
  };
}
function removeTodoAction(id) {
  return {
    type: REMOVE_TODO,
    id,
  };
}
function toggleTodoAction(id) {
  return {
    type: TOOGLE_TODO,
    id,
  };
}

function addGoalAction(goal) {
  return {
    type: ADD_GOAL,
    goal,
  };
}

function removeGoalAction(id) {
  return {
    type: REMOVE_GOAL,
    id,
  };
}

function todos(state = [], action) {
  if (action.type === "ADD_TODO") {
    return state.concat([action.todo]);
  } else if (action.type === REMOVE_TODO) {
    return state.filter((todo) => todo.id !== action.id);
  } else if (action.type === TOOGLE_TODO) {
    return state.map((todo) =>
      todo.id !== action.id
        ? todo
        : Object.assign({}, todo, { complete: !todo.complete })
    );
  } else {
    return state;
  }
}

function goals(state = [], action) {
  switch (action.type) {
    case ADD_GOAL:
      return state.concat([action.goal]);
    case REMOVE_GOAL:
      return state.filter((goal) => goal.id !== action.id);
    default:
      return state;
  }
}

function app(state = {}, action) {
  return {
    todos: todos(state.todos, action),
    goals: goals(state.goals, action),
  };
}

const store = createStore(app);

store.subscribe(() => {
  console.log("the new state is:", store.getState());
});

//
store.dispatch(
  addTodoAction({
    id: 0,
    name: "Learn Redux",
    complete: false,
  })
);

store.dispatch(
  toggleTodoAction({
    id: 0,
  })
);

store.dispatch(
  removeTodoAction({
    id: 0,
  })
);

store.dispatch(
  addGoalAction({
    id: 0,
    name: "Complete MSc",
    complete: false,
  })
);

store.dispatch(
  removeGoalAction({
    id: 0,
  })
);

//this set of code below do the same thing as above. The above is just best practice

store.dispatch({
  type: ADD_GOAL,
  goal: {
    id: 1,
    name: "Get a certification",
    complete: false,
  },
});
store.dispatch({
  type: REMOVE_GOAL,
  goal: {
    id: 1,
  },
});

store.dispatch({
  type: ADD_TODO,
  todo: {
    id: 1,
    name: "Go to the gym",
    complete: true
  }
})
