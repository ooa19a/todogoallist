# TODO and Goal List

### I am completed this project using Redux.

```js
In the TodoList project, I wrote the whole application in one file. This application has the following features

* add an item
* delete an item
* Toggle state of an item
```

I initially wrote this min project using vanilla javascript. I coverted it to React application. 

I learnt how to add React to a redux appliction by passing down the Redux store as props.  

## Short Video Demostration of the Application

| **Screenshots**  | **Screenshots Contd**|
|------------|------------|
| [![homepage](https://media.giphy.com/media/Z9b3BbCPSGMmZuMy7v/giphy.gif)](asset/video_demostration.mp4) | _Click on the gif to see the full (less than 1 minute) video_ |
| [![Homepage](asset/homepage.png)](asset/homepage.png) _**Homepage**_ | [![addedTodo](asset/addedTodo.png)](asset/addedTodo.png) _**added Todo**_ |
| [![Homepage](asset/strikethrough.png)](asset/strikethrough.png) _**toggle complete a todo item**_ | [![addedTodo](asset/deleteTodoItem.png)](asset/deleteTodoItem.png) _**deleted Todo item**_ |
| [![addedGoal](asset/addedGoal.png)](asset/addedGoal.png) _**added Goal**_ | [![deleteGoal](asset/deleteGoal.png)](asset/deleteGoal.png) _**deleted goal item**_ |
| [![displayingconsole](asset/displayingconsole.png)](asset/displayingconsole.png) _**toggle state of an item displayed in console**_ | [![displayingconsole](asset/displayingconsole2.png)](asset/displayingconsole2.png) _**toggle state of an item displayed in console**_ |
